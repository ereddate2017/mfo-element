import { createComponent, createElement, createStyleSheet, Refs, query } from '../../../lib/index'
import { flexCenter } from '../../asset/css'

const dialogStyleSheet = createStyleSheet('.dialog', Object.assign(Object.assign({}, flexCenter), {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,.5)',
    zIndex: '99999'
}))
const dialogPanelStyleSheet = createStyleSheet('.dialog_panel', {
    backgroundColor: '#ffffff',
    width: '50%',
    minHeight: '120px',
    borderRadius: '5px',
    overflow: 'hidden'
})
const dialogHeaderStyleSheet = createStyleSheet('.dialog_header', Object.assign(Object.assign({
    height: '30px',
    padding: '6px 12px',
    color: '#999999'
}, flexCenter), {
    justifyContent: 'left'
}))
const dialogFooterStyleSheet = createStyleSheet('.dialog_footer', Object.assign({
}, flexCenter))
const dialogContentStyleSheet = createStyleSheet('.dialog_content', {
    minHeight: '100px',
    padding: '6px 12px'
})
const dialogButtonStyleSheet = createStyleSheet('.dialog_button', {
    padding: '12px 12px',
    cursor: 'pointer',
    border: '0',
    flex: 1
})
const dialogShowStyleSheet = createStyleSheet('.hide', {
    display: 'none'
})

const dialogBase = createElement('div', {
    class: 'dialog',
    ref: 'dialog_base'
}, createElement('div', {
    class: 'dialog_panel',
}, createElement('div', {
    class: 'dialog_header',
    ref: 'dialog_header',
    text: 'Title'
}), createElement('div', {
    class: 'dialog_content',
    ref: 'dialog_content'
}), createElement('div', {
    class: 'dialog_footer'
}, createElement('button', {
    class: 'dialog_button cancel',
    ref: 'dialog_cancel',
    text: 'Cancel',
    onclick() {
        const that = Refs('dialog_base')
        query(that[0]).addClass('hide')
    }
}), createElement('button', {
    class: 'dialog_button enter',
    ref: 'dialog_enter',
    text: 'Enter',
    onclick() {
        const that = Refs('dialog_base')
        query(that[0]).addClass('hide')
    }
}))))

export const dialogControl = (ops) => {
    window.document.body.append(dialog.elem)
    Object.keys(ops).forEach(item => {
        switch (item) {
            case 'components':
                const content = Refs('dialog_content');
                ops[item].forEach(comp => content[0].append(comp.item))
                break;
            case 'title':
                const title = Refs('dialog_header')
                title[0].childNodes[0].textContent = ops[item]
                break;
            case 'buttons':
                Object.keys(ops[item]).forEach(button => {
                    Object.keys(ops[item][button]).forEach(key => {
                        const customButton = Refs('dialog_' + button)
                        if (!customButton[0]) return;
                        switch (key) {
                            case 'text':
                                customButton[0].childNodes[0].textContent = ops[item][button][key]
                                break;
                            case 'onclick':
                                query(customButton[0]).on('click', ops[item][button][key])
                                break;
                            case 'show':
                                query(customButton[0]).show()
                                break;
                            case 'hide':
                                query(customButton[0]).hide()
                                break;
                        }
                    })
                })
                break;
        }
    })
    return dialog.elem;
}

export const dialog = createComponent('dialog', {
    render() {
        return dialogBase
    }
})