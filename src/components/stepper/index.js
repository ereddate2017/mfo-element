import { Ref, createStyleSheet, createElement, createComponent } from '../../../lib/index'
import { flexCenter } from '../../asset/css'

const data = new Ref({
    n: {
        type: Number,
        default() {
            return 0
        }
    },
    total: {
        type: Number,
        default() {
            return 10
        }
    },
    mini: {
        type: Number,
        default() {
            return 0
        }
    },
    disabled: {
        type: String,
        default() {
            return ''
        }
    }
});

const stepperStyleSheet = createStyleSheet('.stepper', Object.assign({

}, flexCenter))
const buttonStyleSheet = createStyleSheet(Object.assign({
    padding: '6px 12px',
    backgroundColor: '#efefef',
    cursor: 'pointer'
}, flexCenter));
const inputStyleSheet = createStyleSheet(Object.assign({
    padding: '6px 12px',
    backgroundColor: '#efefef',
    width: '60px',
    textAlign: 'center'
}, flexCenter));

export const stepper = createComponent('stepper', {
    render() {
        return createElement('div', { class: 'stepper' }, createElement('button', {
            text: '+',
            ref: 'v-increase',
            style: buttonStyleSheet,
            onclick(event) {
                if (data.n + 1 < data.total) {
                    data.n += 1;
                    data.updated(() => {
                        const ref = Refs('v-input');
                        ref[0].value = `${data.n}`;
                        const increase = Refs('v-increase');
                        const decrease = Refs('v-decrease');
                        increase[0].disabled = decrease[0].disabled = '';
                    })
                } else {
                    data.disabled = 'disabled'
                    data.updated(() => {
                        const increase = Refs('v-increase');
                        increase[0].disabled = data.disabled;
                        const decrease = Refs('v-decrease');
                        decrease[0].disabled = '';
                    })
                }
            }
        }), createElement('input', {
            value: `${data.n}`,
            ref: 'v-input',
            style: inputStyleSheet,
            readOnly: true,
            onchange(evt) {
                data.n = parseFloat(evt.target.value);
            }
        }), createElement('button', {
            text: '-',
            ref: 'v-decrease',
            disabled: data.disabled,
            style: buttonStyleSheet,
            onclick(event) {
                if (data.n - 1 > data.mini) {
                    data.n -= 1;
                    data.updated(() => {
                        const ref = Refs('v-input');
                        ref[0].value = `${data.n}`;
                        const increase = Refs('v-increase');
                        const decrease = Refs('v-decrease');
                        increase[0].disabled = decrease[0].disabled = '';
                    })
                } else {
                    data.disabled = ''
                    data.updated(() => {
                        const increase = Refs('v-increase');
                        increase[0].disabled = data.disabled;
                        const decrease = Refs('v-decrease');
                        decrease[0].disabled = 'disabled';
                    })
                }
            }
        }))
    }
});