import { createComponent, createElement, createStyleSheet } from '../../../lib/index'
import { flexCenter } from '../../asset/css'

const panelStyleSheet = createStyleSheet('.panel', Object.assign({
    padding: '6px 12px',
    backgroundColor: '#efefef',
    marginTop: '60px'
}, flexCenter));

export const layout = createComponent('panel', {
    render() {
        return createElement('div', {
            ref: 'router-view',
            class: 'panel'
        })
    }
})