import { createElement, createStyleSheet, createComponent, query, Refs } from '../../../lib/index'
import { flexCenter, positionFix } from '../../asset/css/index'

const gotopStyleSheet = createStyleSheet('.gotop', Object.assign(positionFix, Object.assign(Object.assign({}, flexCenter), {
    width: '32px',
    height: '32px',
    borderRadius: '32px',
    backgroundColor: 'rgba(0,0,0,.5)',
    bottom: '60px',
    right: '20px',
    left: 'auto',
    cursor: 'pointer',
    color: '#ffffff',
    display: 'none',
    zIndex: '99999'
})))

query(window).on('scroll', () => {
    const top = document.documentElement.scrollTop;
    const dom = Refs('gotop')
    if (top >= 100) {
        query(dom[0]).show()
    } else {
        query(dom[0]).hide()
    }
});
export const gotop = createComponent('gotop', {
    render() {
        return createElement('div', {
            class: 'gotop',
            ref: 'gotop',
            onclick() {
                window.scrollTo(1, 1)
            }
        })
    }
})