import { createComponent, createElement, createStyleSheet, Ref } from '../../../lib/index'
import { flexCenter, positionFix } from '../../asset/css'
import { router } from '../../router/index'

const data = new Ref({
    headTitle: {
        type: String,
        default() {
            return 'TITLE'
        }
    }
});

const button = createElement('span', {
    class: 'icon-font',
    onclick() {
        console.log('1')
    }
});

const goButton = createComponent('goButton', {
    render() {
        return button.update({
            text: 'left',
            onclick() {
                console.log('2')
                router.push({ path: '/' })
            }
        })
    }
})
const moreButton = createComponent('moreButton', {
    render() {
        return button.update({
            text: 'right',
            onclick() {
                console.log('3')
                router.push({ path: '/user' })
            }
        })
    }
})

const headerStyleSheet = createStyleSheet('.header', Object.assign(Object.assign({}, positionFix), Object.assign({
    padding: '0 12px',
    backgroundColor: '#ffffff',
    height: '60px',
    top: 0,
    zIndex: '99999'
}, flexCenter)));
const headerItemStyleSheet = createStyleSheet('.header_item', {
    padding: '6px 12px',
    width: '99%',
    textAlign: 'center'
});

export const header = createComponent('header', {
    render() {
        return createElement('header', {
            class: 'header'
        }, createElement('div', {
            class: 'header_item',
            style: createStyleSheet({
                width: '100px'
            }),
        }, goButton), createElement('div', {
            class: 'header_item',
            ref: 'headerTitle',
            text: `${data.headTitle}`
        }), createElement('div', {
            class: 'header_item',
            style: createStyleSheet({
                width: '100px'
            }),
        }, moreButton))
    }
})