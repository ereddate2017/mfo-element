import { createComponent, createElement, createStyleSheet } from '../../../lib/index'
import { flexCenter } from '../../asset/css'

const footerStyleSheet = createStyleSheet('.footer', Object.assign({
    padding: '0 12px',
    height: '60px',
    textAlign: 'center',
    color: '#999999'
}, flexCenter))

export const footer = createComponent('footer', {
    render() {
        return createElement('footer', {
            class: 'footer',
            text: 'footer'
        })
    }
})