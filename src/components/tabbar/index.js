import { createComponent, createStyleSheet, createElement, query } from '../../../lib/index'
import { routes, router } from '../../router/index'
import { flexCenter, positionFix } from '../../asset/css';

const tabbarStyleSheet = createStyleSheet('.tabbar', Object.assign(Object.assign({
    bottom: 0,
    backgroundColor: '#ffffff',
    zIndex: '99999'
}, flexCenter), positionFix))
const tabItemStyleSheet = createStyleSheet('.tab_item', Object.assign({
    width: 100 / routes.length + '%',
    textAlign: 'center',
    padding: "6px 12px",
    height: '30px',
    cursor: 'pointer'
}, flexCenter))
const tabItemCurrentStyleSheet = createStyleSheet('.tab_item.on', {
    color: '#09bb07',
    fontWeight: 700
})

const tabs = [];
routes.forEach((item, i) => {
    const insertItem = createElement('div', {
        class: 'tab_item',
        ref: 'tab_ref_' + item.path,
        text: `${item.meta.title}`,
        onclick() {
            router.push({ path: item.path })
        }
    });
    tabs.push(insertItem.elem)
})

export const tabbar = createComponent('tabbar', {
    render() {
        return createElement('div', {
            class: 'tabbar',
            components: tabs
        })
    }
})