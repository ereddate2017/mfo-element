import { createElement, createStyleSheet } from '../../../lib/index'

const buttonStyleSheet = createStyleSheet('.button', {
    display: 'block',
    cursor: 'pointer'
})
export const button = (ops) => {
    const data = {
        class: 'button',
        text: 'button',
        onclick() {

        }
    }
    const component = createElement('button', data)
    component.update(ops)
    return component
}