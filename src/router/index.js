import { query, Refs } from '../../lib/index'
import { createRouter } from 'mfo-router/lib/index'
import { routerHomeRender } from '../view/home/index'
import { routerUserRender } from '../view/user/index'

const switchTab = (current) => {
    const headerTitle = Refs('headerTitle')
    const title = current.meta.title
    query(headerTitle[0]).html(title)
    query('title').html(title)

    routes.forEach(route => {
        const tab = Refs('tab_ref_' + route.path)
        query(tab[0]).removeClass('on')
    })
    const tab = Refs('tab_ref_' + current.path)
    query(tab[0]).addClass('on')
}

export const routes = [{
    name: 'home',
    path: '/',
    render() {
        routerHomeRender(this)
    },
    meta: {
        title: 'Home',
    }
}, {
    name: 'user',
    path: '/user',
    render() {
        routerUserRender(this)
    },
    meta: {
        title: 'User',
    }
}]

const router = createRouter({
    mode: 'hash',
    routes
})

router.beforeEach((from, to, next) => {
    next()
})

router.afterEach((from, to) => {
    switchTab(to)
})

export {
    router
};