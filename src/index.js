import { createComponent, createElement, createStyleSheet } from '../lib/index'
import { layout } from './components/layout'
import { header } from './components/header'
import { footer } from './components/footer'
import { tabbar } from './components/tabbar'
import { gotop } from './components/gotop'
import { dialog, dialogControl } from './components/dialog'
import { router } from './router/index'

const bodyStyleSheet = createStyleSheet('html,body,li,ol,ul,p,h1,h2,h3,h4,h5,h6,header,footer', {
    padding: '0',
    margin: '0',
    fontFamily: 'arial',
    listStyle: 'none',
    backgroundColor: '#efefef',
    fontSize: '14px',
    display: 'block'
});

const appStyleSheet = createStyleSheet('.app', {
    display: 'block'
});

const App = createComponent('app', {
    render() {
        return createElement('div', {
            class: 'app'
        }, header, layout, footer, tabbar, gotop, dialog)
    }
})

document.body.appendChild(App.elem)

dialogControl({
    title: 'Dialog Title',
    components: [],
    buttons: {
        cancel: {
            text: '取消',
            onclick() {
                console.log('cancel')
            }
        },
        enter: {
            text: '确定',
            onclick() {
                console.log('enter')
            }
        }
    }
})
router.forceUpdate()
