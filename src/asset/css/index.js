export const flexCenter = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}

export const positionFix = {
    position: 'fixed',
    left: 0,
    right: 0
}