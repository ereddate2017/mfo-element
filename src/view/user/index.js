import { createElement, Refs, createComponent } from '../../../lib/index'

export const user = createComponent('user', {
    render() {
        return createElement('div', {
            class: 'user',
            text: 'User'
        })
    }
});

export const routerUserRender = (route) => {
    const routerView = Refs('router-view')
    routerView[0].innerHTML = '';
    routerView[0].append(user.elem)
}