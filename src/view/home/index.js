import { createElement, Refs, createComponent } from '../../../lib/index'
import { stepper } from '../../components/stepper/index'
import { button } from '../../components/button/index'

export const home = createComponent('home', {
    render() {
        return createElement('div', {
            class: 'user'
        }, stepper, button({
            text: 'enter button',
            onclick() {
                console.log('button')
            }
        }))
    }
});

export const routerHomeRender = (route) => {
    const routerView = Refs('router-view')
    routerView[0].innerHTML = '';
    routerView[0].append(home.elem)
}