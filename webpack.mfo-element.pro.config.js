const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
    mode: 'production',
    entry: [path.resolve(__dirname, 'lib', 'index.js'),],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'lib/mfo-element.[hash:8].js'
    },
    plugins: [
        new CleanWebpackPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/,
                use: ['babel-loader'],
                include: path.resolve(__dirname, 'src'),
                exclude: /node_modules/
            }
        ]
    }
}